$(function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});
$('.carousel').carousel({
  interval: 3000
});

$('#btnSuscrip').on('show.bs.modal', function(e) {
  $("#btnAccionSus").prop('disabled', true);
  $("#btnAccionSus").removeClass("btn-primary");
  $("#btnAccionSus").addClass("btn-danger");
  console.log("Abriendo modal");
})

$('#btnSuscrip').on('shown.bs.modal', function(e) {
  console.log("Modal abierto");
})

$('#btnSuscrip').on('hide.bs.modal', function(e) {
  $("#btnAccionSus").prop('disabled', false);
  $("#btnAccionSus").removeClass("btn-danger");
  $("#btnAccionSus").addClass("btn-primary");
  console.log("Cerrando modal");
})

$('#btnSuscrip').on('hidden.bs.modal', function(e) {
  console.log("Modal cerrado");
})